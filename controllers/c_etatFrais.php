﻿<?php
// include("views/v_sommaire.php");
$action = $_REQUEST['action'];
$idVisiteur = $_SESSION['idVisiteur'];
switch ($action) {
	case 'acceuil': {
			include("views/v_sommaire.php");
			break;
		}
	case 'AfficherSaisiePeriode': {

			include("views/v_ajouterPeriode.php");
			break;
		}
	case "saisiePeriode": {
			$mois = $_POST["mois"];
			$annee = $_POST["annee"];

			$formatMois = strlen($mois) < 2 ? (intval($mois) < 10 ? "0$mois" : $mois) : $mois;

			$pdo->addFicheFrais($idVisiteur, "$annee$formatMois");

			include("views/v_ajouterFrais.php");
			break;
		}

	case "saisieFrais": {
			$mois = $_POST["mois"];

			$TABLEAU = [];

			$TABLEAU["REP"] = $_POST["REP"];
			$TABLEAU["NUI"] = $_POST["NUI"];
			$TABLEAU["ETP"] = $_POST["ETP"];
			$TABLEAU["KM"] = $_POST["KM"];

			foreach ($TABLEAU as $idFraisForfait => $quantite) {
				$resultat =	$pdo->addFrais($idVisiteur, "$mois", $idFraisForfait, $quantite);
				print($resultat);
			}

			break;
		}
	case 'selectionnerMois': {
			$lesMois = $pdo->getLesMoisDisponibles($idVisiteur);
			// Afin de sélectionner par défaut le dernier mois dans la zone de liste
			// on demande toutes les clés, et on prend la première,
			// les mois étant triés décroissants
			$lesCles = array_keys($lesMois);
			$moisASelectionner = $lesCles[0];
			include("views/v_listeMois.php");
			break;
		}
	case 'voirEtatFrais': {
			$leMois = $_POST['lstMois'];
			$lesMois = $pdo->getLesMoisDisponibles($idVisiteur);
			$moisASelectionner = $leMois;
			include("views/v_listeMois.php");
			$lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $leMois);
			$lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $leMois);
			$numAnnee = substr($leMois, 0, 4);
			$numMois = substr($leMois, 4, 2);
			$libEtat = $lesInfosFicheFrais['libEtat'];
			$montantValide = $lesInfosFicheFrais['montantValide'];
			$nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
			$dateModif =  $lesInfosFicheFrais['dateModif'];
			$dateModif =  dateAnglaisVersFrancais($dateModif);
			include("views/v_etatFrais.php");
		}
}
