<form action="index.php?uc=etatFrais&action=saisiePeriode" method="POST">
    <h3>Saisie d'une période</h3>
    <div class="mb-3 row">

        <label for="" class="col-sm-2 col-form-label"><b>Visiteur:</b></label>

        <div class="col-sm-10">
            <label for="" class="col-sm-2 col-form-label">Numero:</label>
            <input name="idVisiteur" type="text" class="form-control" value=<?= $idVisiteur ?> disabled />
        </div>

        <label for="" class="col-sm-2 col-form-label"><b>PERIODE D'ENGAGEMENT:</b></label>
        <div class="col-sm-10">
            <label for="" class="col-sm-2 col-form-label">Mois (2 chiffres):</label>
            <input name="mois" value="<?php echo date('m'); ?>" max="12" type="number" class="form-control" />

            <label for="" class="col-sm-2 col-form-label">Année (4 chiffres):</label>
            <input name="annee" min="<?php echo intval(date('Y')); ?>" max="<?php echo intval(date('Y')) + 2; ?>" type="number" value="<?php echo date('Y'); ?>" class="form-control" />
        </div>

        <div class="col-sm-10">
            <input type="submit" value="Valider" />
        </div>
    </div>
</form>