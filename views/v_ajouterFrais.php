<form action="index.php?uc=etatFrais&action=saisieFrais" method="POST">
    <h3>Saisie d'un frais</h3>
    <div class="mb-3 row">
        <input hidden name="mois" value="<?= $annee.$formatMois ?>" />

        <div class="col-sm-10">
            <label for="" class="col-sm-2 col-form-label"><b>Repas midi:</b></label>
            <input name="REP" type="number" class="form-control" value="0">
        </div>

        <div class="col-sm-10">
            <label for="" class="col-sm-2 col-form-label">Nuitées:</label>
            <input name="NUI" type="number" class="form-control" value="0">
        </div>

        <div class="col-sm-10">
            <label for="" class="col-sm-2 col-form-label"><b>Etape:</b></label>
            <input name="ETP" type="number" class="form-control" value="0">
        </div>

        <div class="col-sm-10">
            <label for="" class="col-sm-2 col-form-label">Km:</label>
            <input name="KM" type="number" class="form-control" value="0">
        </div>

        <div class="col-sm-10">
            <input type="submit" value="Valider" />
        </div>

    </div>
    </div>
</form>