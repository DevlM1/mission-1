﻿    <!-- Division pour le sommaire -->
    <h3>Sommaire</h3>
    <nav class="menuLeft">
       <ul class="menu-ul">
          <?php if (!empty($_SESSION['prenom'])) { ?>
             <li class="menu-item">
                Bonjour <b><?php echo $_SESSION['prenom'] . "  " . $_SESSION['nom']  ?></b> bienvenue sur GSB
             </li>
          <?php } ?>
          <p>Que voulez vous faire ?</p>
          <li class="menu-item">
             <a href="index.php?uc=etatFrais&action=AfficherSaisiePeriode" title="Saisir une période et des frais">Saisir une période et des frais</a>
          </li>
          <li class="menu-item">
             <a href="index.php?uc=etatFrais&action=selectionnerMois" title="Consultation de mes fiches de frais">Mes fiches de frais</a>
          </li>
       </ul>
    </nav>